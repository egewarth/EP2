/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import controller.CamaraModel;
import edu.unb.fga.dadosabertos.Deputado;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import static jdk.nashorn.internal.objects.NativeString.toUpperCase;

/**
 *
 * @author joaoegewarth
 */
public class EP2 {
    /**
     * @param args the command line arguments
     */
    private CamaraModel camara = new CamaraModel();
    private List<Deputado> listaOrdenaNome;
    private List<Deputado> listaOrdenaPartido;
    private List<Deputado> listaOrdenaEstado;
    private List<Deputado> listaResultados;
    
    public void buscaNome(String nome){
        listaResultados = camara.getList();
        int i = 0, j = 0;
        while(camara.getList()==null);
        for(i = 0; i < camara.getList().size();i++){
            if(camara.getList().get(i).getNome().startsWith(toUpperCase(nome)) == true){
                listaResultados.set(j, camara.getList().get(i));
                j++;
            }
        }
        System.out.println(listaResultados.size());
        for(j = j; j != listaResultados.size(); ){
            listaResultados.remove(j);
        }
    }
    public void buscaPartido(String partido){
        listaResultados = camara.getList();
        int i = 0, j = 0;
        while(camara.getList()==null);
        for(i = 0; i < camara.getList().size();i++){
            if(toUpperCase(camara.getList().get(i).getPartido()).startsWith(toUpperCase(partido)) == true){
                listaResultados.set(j, camara.getList().get(i));
                j++;
            }
        }
        System.out.println(listaResultados.size());
        for(j = j; j != listaResultados.size(); ){
            listaResultados.remove(j);
        }
    }
////////////////////////////////////////////////////////////////////////////////  
    public DefaultTableModel criaTabelaCompleta(List<Deputado> deputadosTabela) {
        DefaultTableModel tabela = new DefaultTableModel();
        
        deputadosTabela.toArray();
        
        String[] nomeTable = new String[deputadosTabela.size()];
        String[] partidoTable = new String[deputadosTabela.size()];
        String[] estadoTable = new String[deputadosTabela.size()];
        String[] condicaoTable = new String[deputadosTabela.size()];
        
        for(int i = 0; i < deputadosTabela.size();i++){
            nomeTable[i] = deputadosTabela.get(i).getNome();
            partidoTable[i] = deputadosTabela.get(i).getPartido();
            estadoTable[i] = deputadosTabela.get(i).getUf();
            condicaoTable[i] = deputadosTabela.get(i).getCondicao();
            
        }
        tabela.addColumn("Nome", nomeTable);
        tabela.addColumn("Partido", partidoTable);
        tabela.addColumn("Estado", estadoTable);
        tabela.addColumn("Condicao", condicaoTable);
        
         return tabela;
    }
    public CamaraModel getCamara(){
        return camara;
    }
    public void ordenaNome(){ 
        Deputado auxiliar;
        //List<Deputado> auxiliarList = camara.getList();
        
        listaOrdenaNome = this.camara.getList();
        
        listaOrdenaNome.toArray();
        int i, j;
        for( i = 1; i < listaOrdenaNome.size(); i++){
            auxiliar = listaOrdenaNome.get(i);
            for( j = i - 1; j >= 0; j--){
                if(auxiliar.getNome().compareTo(listaOrdenaNome.get(j).getNome())< 0){
                    this.listaOrdenaNome.set(j + 1, listaOrdenaNome.get(j));
                }else{
                    break;
                }
            }
            this.listaOrdenaNome.set(j+1, auxiliar);
            //auxiliarList = camara.getList();
        }
    }
    public void ordenaPartido(){ 
        Deputado auxiliar;
        //List<Deputado> auxiliarList = camara.getList();
        
        listaOrdenaPartido = this.camara.getList();
        
        listaOrdenaPartido.toArray();
        int i, j;
        for( i = 1; i < listaOrdenaPartido.size(); i++){
            auxiliar = listaOrdenaPartido.get(i);
            for( j = i - 1; j >= 0; j--){
                if(auxiliar.getPartido().compareTo(listaOrdenaPartido.get(j).getPartido())< 0){
                    this.listaOrdenaPartido.set(j + 1, listaOrdenaPartido.get(j));
                }else{
                    break;
                }
            }
            this.listaOrdenaPartido.set(j+1, auxiliar);
            //auxiliarList = camara.getList();
        }
    }
    public void ordenaEstado(){ 
        Deputado auxiliar;
        //List<Deputado> auxiliarList = camara.getList();
        
        listaOrdenaEstado = this.camara.getList();
        
        listaOrdenaEstado.toArray();
        int i, j;
        for( i = 1; i < listaOrdenaEstado.size(); i++){
            auxiliar = listaOrdenaEstado.get(i);
            for( j = i - 1; j >= 0; j--){
                if ((toUpperCase(auxiliar.getUf())).compareTo(listaOrdenaPartido.get(j).getUf())< 0){
                    this.listaOrdenaEstado.set(j + 1, listaOrdenaEstado.get(j));
                }else{
                    break;
                }
            }
            this.listaOrdenaEstado.set(j+1, auxiliar);
            //auxiliarList = camara.getList();
        }
    }
    public List<Deputado> getListBusca() {
        if(listaResultados.isEmpty() == true){
            return null;
        }else{
            return listaResultados;
        }
    }
    public List<Deputado> getListBuscaPartido() {
        if(listaResultados.isEmpty() == true){
            return null;
        }else{
            return listaResultados;
        }
    }
    public List<Deputado> getListOrdenaEstado() {
        if(listaOrdenaEstado.isEmpty() == true){
            System.out.println("lista vazia");
            System.exit(1);
            return null;
        }else{
            return listaOrdenaEstado;
        }
    }
    public List<Deputado> getListOrdenaPartido() {
        if(listaOrdenaPartido.isEmpty() == true){
            System.out.println("lista vazia");
            System.exit(1);
            return null;
        }else{
            return listaOrdenaPartido;
        }
    }
    public List<Deputado> getListOrdenaNome() {
        if(listaOrdenaNome.isEmpty() != true){
            return listaOrdenaNome;
        }else{
            System.out.println("lista vazia");
            System.exit(1);
            return null;
        }
    }
    public static void main(String[] args) {
        // TODO code application logic here
       //CamaraModel camara = new CamaraModel();
       //camara.imprime();
       //camara.imprimeOrdenado();
       //camara.buscaNome(null);
    }
    
}
