/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;
import java.io.IOException;
import java.util.*;  
import java.text.*;
import javax.xml.bind.JAXBException;
/**
 *
 * @author joaoegewarth
 */
public class CamaraModel implements Runnable {

    private Camara listaDeputados;
    private List<Deputado> deputadosList;
    

    public CamaraModel() {
        listaDeputados = new Camara();
        deputadosList = null;
        this.run();
        deputadosList = listaDeputados.getDeputados();
    }

    /**
     *
     */
    @Override
    public synchronized void run() {
        try {
             listaDeputados.obterDados();
        }catch (JAXBException jaxbException){
            //abrir painel de alerta com ok;
        }catch (IOException camaraExeption){
            //abrir painel;
        }
        
    }
    public List<Deputado> getList() {
        if(deputadosList.isEmpty() == true){
            System.out.println("lista vazia");
            System.exit(1);
            return null;
        }else{
            return deputadosList;
        }
    }
}
